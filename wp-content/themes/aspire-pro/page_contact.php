<?php

// Template Name: Contact

// Adds Page Title
//add_action( 'genesis_before_content', 'genesis_do_post_title' );

//Adds Page Content
add_action( 'genesis_before_loop', 'aspire_do_portfolio_content' );
function aspire_do_portfolio_content() {
    echo '<div class="entry-content entry-portfolio contact-page" itemprop="text">';
		while ( have_posts() ) : the_post();
			the_content();
			echo '<div class="custom_wrapper">';
			the_field('contact_form_code','option');
			echo '</div>';
		endwhile;
	  echo '</div>';
}
		
// Force layout to full-width-content
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );


// Remove standard loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

	
?>


 <body <?php body_class('contact_custom'); ?>> 

<?php
	
genesis();		
?>
