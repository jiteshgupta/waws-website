<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'aspire', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'aspire' ) );

//* Add Image upload and Color select to WordPress Theme Customizer
require_once( get_stylesheet_directory() . '/lib/customize.php' );

//* Include Customizer CSS
include_once( get_stylesheet_directory() . '/lib/output.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Aspire Pro' );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/aspire/' );
define( 'CHILD_THEME_VERSION', '1.2' );

//* Enqueue Scripts
add_action( 'wp_enqueue_scripts', 'aspire_enqueue_scripts_styles' );
function aspire_enqueue_scripts_styles() {

	wp_enqueue_script( 'aspire-fadeup-script', get_stylesheet_directory_uri() . '/js/fadeup.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_script( 'aspire-global', get_bloginfo( 'stylesheet_directory' ) . '/js/global.js', array( 'jquery' ), '1.0.0' );

}

//* Add Font Awesome Support
add_action( 'wp_enqueue_scripts', 'enqueue_font_awesome' );
function enqueue_font_awesome() {
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array(), '4.5.0' );
}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

//* Add accessibility support
add_theme_support( 'genesis-accessibility', array( 'drop-down-menu', 'search-form', 'skip-links' ) );

//* Add WooCommerce Support
add_theme_support( 'genesis-connect-woocommerce' );

//* Enqueue custom WooCommerce styles when WooCommerce active
add_filter( 'woocommerce_enqueue_styles', 'aspire_woocommerce_styles' );
function aspire_woocommerce_styles( $enqueue_styles ) {

	$enqueue_styles['aspire-woocommerce-styles'] = array(
		'src'     => get_stylesheet_directory_uri() . '/woocommerce/css/woocommerce.css',
		'deps'    => '',
		'version' => CHILD_THEME_VERSION,
		'media'   => 'screen'
	);

	return $enqueue_styles;

}

// Change number or products per row to 4
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 4; // 4 products per row
	}
}

// WooCommerce | Display 30 products per page.
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 30;' ), 20 );

// Enable WooCommerce Gallery Features (Zoom, Lightbox, Slider)
add_action( 'after_setup_theme', 'yourtheme_setup' );
function yourtheme_setup() {
	/*add_theme_support( 'wc-product-gallery-zoom' );*/
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'flex-height'     => true,
	'width'           => 300,
	'height'          => 60,
	'header-selector' => '.site-title a',
	'header-text'     => false,
) );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'footer-widgets',
	'footer',
) );

//* Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

//* Add new image sizes
add_image_size( 'featured-content-lg', 1200, 600, TRUE );
add_image_size( 'featured-content-sm', 600, 400, TRUE );
add_image_size( 'featured-content-th', 600, 600, TRUE );
add_image_size( 'portfolio-thumbnail', 348, 240, TRUE );

//* Unregister layout settings
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

//* Unregister secondary sidebar
unregister_sidebar( 'sidebar-alt' );

//* Unregister the header right widget area
unregister_sidebar( 'header-right' );

//* Reposition the primary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

//* Remove output of primary navigation right extras
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

//* Unregister secondary navigation menu
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Primary Navigation Menu', 'genesis' ) ) );

// Add featured image above the entry content
/*add_action( 'genesis_before_entry', 'aspire_featured_photo' );
function aspire_featured_photo() {

    if ( ! ( is_singular() && has_post_thumbnail() ) ) {
        return;
    }

    $image = genesis_get_image( 'format=url&size=single-posts' );

    $thumb_id = get_post_thumbnail_id( get_the_ID() );
    $alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );

    if ( '' == $alt ) {
        $alt = the_title_attribute( 'echo=0' );
    }

    printf( '<div class="featured-image"><img src="%s" alt="%s" class="entry-image"/></div>', esc_url( $image ), $alt );
}*/

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

//* Add support for footer menu
add_theme_support ( 'genesis-menus' , array ( 'primary' => 'Primary Navigation Menu', 'footer' => 'Footer Navigation Menu' ) );

//* Hook menu in footer
add_action( 'genesis_footer', 'aspire_footer_menu', 7 );
function aspire_footer_menu() {
	printf( '<nav %s>', genesis_attr( 'nav-footer' ) );
	wp_nav_menu( array(
		'theme_location' => 'footer',
		'container'      => false,
		'depth'          => 1,
		'fallback_cb'    => false,
		'menu_class'     => 'genesis-nav-menu',
	) );

	echo '</nav>';
}

// Theme Settings init
add_action( 'admin_menu', 'aspire_theme_settings_init', 15 );
/**
 * This is a necessary go-between to get our scripts and boxes loaded
 * on the theme settings page only, and not the rest of the admin
 */
function aspire_theme_settings_init() {
    global $_genesis_admin_settings;

    add_action( 'load-' . $_genesis_admin_settings->pagehook, 'aspire_add_portfolio_settings_box', 20 );
}

// Add Portfolio Settings box to Genesis Theme Settings
function aspire_add_portfolio_settings_box() {
    global $_genesis_admin_settings;

    add_meta_box( 'genesis-theme-settings-aspire-portfolio', __( 'Portfolio Page Settings', 'aspire' ), 'aspire_theme_settings_portfolio',     $_genesis_admin_settings->pagehook, 'main' );
}

/**
 * Adds Portfolio Options to Genesis Theme Settings Page
 */
function aspire_theme_settings_portfolio() { ?>

	<p><?php _e("Display which category:", 'genesis'); ?>
	<?php wp_dropdown_categories(array('selected' => genesis_get_option('aspire_portfolio_cat'), 'name' => GENESIS_SETTINGS_FIELD.'[aspire_portfolio_cat]', 'orderby' => 'Name' , 'hierarchical' => 1, 'show_option_all' => __("All Categories", 'genesis'), 'hide_empty' => '0' )); ?></p>

	<p><?php _e("Exclude the following Category IDs:", 'genesis'); ?><br />
	<input type="text" name="<?php echo GENESIS_SETTINGS_FIELD; ?>[aspire_portfolio_cat_exclude]" value="<?php echo esc_attr( genesis_get_option('aspire_portfolio_cat_exclude') ); ?>" size="40" /><br />
	<small><strong><?php _e("Comma separated - 1,2,3 for example", 'genesis'); ?></strong></small></p>

	<p><?php _e('Number of Posts to Show', 'genesis'); ?>:
	<input type="text" name="<?php echo GENESIS_SETTINGS_FIELD; ?>[aspire_portfolio_cat_num]" value="<?php echo esc_attr( genesis_option('aspire_portfolio_cat_num') ); ?>" size="2" /></p>

	<p><span class="description"><?php _e('<b>NOTE:</b> The Portfolio Page displays the "Portfolio Page" image size plus the excerpt or full content as selected below.', 'aspire'); ?></span></p>

	<p><?php _e("Select one of the following:", 'genesis'); ?>
	<select name="<?php echo GENESIS_SETTINGS_FIELD; ?>[aspire_portfolio_content]">
		<option style="padding-right:10px;" value="full" <?php selected('full', genesis_get_option('aspire_portfolio_content')); ?>><?php _e("Display post content", 'genesis'); ?></option>
		<option style="padding-right:10px;" value="excerpts" <?php selected('excerpts', genesis_get_option('aspire_portfolio_content')); ?>><?php _e("Display post excerpts", 'genesis'); ?></option>
	</select></p>

	<p><label for="<?php echo GENESIS_SETTINGS_FIELD; ?>[aspire_portfolio_content_archive_limit]"><?php _e('Limit content to', 'genesis'); ?></label> <input type="text" name="<?php echo GENESIS_SETTINGS_FIELD; ?>[aspire_portfolio_content_archive_limit]" id="<?php echo GENESIS_SETTINGS_FIELD; ?>[aspire_portfolio_content_archive_limit]" value="<?php echo esc_attr( genesis_option('aspire_portfolio_content_archive_limit') ); ?>" size="3" /> <label for="<?php echo GENESIS_SETTINGS_FIELD; ?>[aspire_portfolio_content_archive_limit]"><?php _e('characters', 'genesis'); ?></label></p>

	<p><span class="description"><?php _e('<b>NOTE:</b> Using this option will limit the text and strip all formatting from the text displayed. To use this option, choose "Display post content" in the select box above.', 'genesis'); ?></span></p>
<?php
}

// Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'front-page-1',
	'name'        => __( 'Front Page 1', 'aspire' ),
	'description' => __( 'This is the front page 1 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-2',
	'name'        => __( 'Front Page 2', 'aspire' ),
	'description' => __( 'This is the front page 2 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-3',
	'name'        => __( 'Front Page 3', 'aspire' ),
	'description' => __( 'This is the front page 3 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-4',
	'name'        => __( 'Front Page 4', 'aspire' ),
	'description' => __( 'This is the front page 4 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-mid-left',
	'name'        => __( 'Home Mid Left Sidebar', 'aspire' ),
	'description' => __( 'This is the Home Mid Left section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-mid-right',
	'name'        => __( 'Home Mid Right Sidebar', 'aspire' ),
	'description' => __( 'This is the Home Mid Right section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-mid-wide',
	'name'        => __( 'Home Mid Wide Sidebar', 'aspire' ),
	'description' => __( 'This is the Home Mid Wide section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-5',
	'name'        => __( 'Front Page 5', 'aspire' ),
	'description' => __( 'This is the front page 5 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-6',
	'name'        => __( 'Front Page 6', 'aspire' ),
	'description' => __( 'This is the front page 6 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-7',
	'name'        => __( 'Front Page 7', 'aspire' ),
	'description' => __( 'This is the front page 7 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-8',
	'name'        => __( 'Front Page 8', 'aspire' ),
	'description' => __( 'This is the front page 8 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-9',
	'name'        => __( 'Front Page 9', 'aspire' ),
	'description' => __( 'This is the front page 9 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-10',
	'name'        => __( 'Front Page 10', 'aspire' ),
	'description' => __( 'This is the front page 10 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-11',
	'name'        => __( 'Front Page 11', 'aspire' ),
	'description' => __( 'This is the front page 11 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-12',
	'name'        => __( 'Front Page 12', 'aspire' ),
	'description' => __( 'This is the front page 12 section.', 'aspire' ),
) );
genesis_register_sidebar( array(
	'id'          => 'front-page-13',
	'name'        => __( 'Front Page 13', 'aspire' ),
	'description' => __( 'This is the front page 13 section.', 'aspire' ),
) );

/***Redirect To custom page in Contact Form 7 */
function add_contactform_redirection(){ ?>
 <script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = '<?php echo home_url('thankyou');?>';
}, false );
</script>
 <?php } 
 add_action('wp_footer', 'add_contactform_redirection'); 
 /***Redirect To custom page in Contact Form 7 */
 

 function fs_custom_content_types() {
	  // testimonials
register_post_type( 'testimonials',
	array(
			'labels' => array(
			'name' => ( 'Testimonials' ),
			'singular_name' => ( 'Testimonial' )
		),
		'public' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'testimonials'),
		'menu_icon' => 'dashicons-format-quote',
		'supports' => array(
		'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes'
			)
		)
);
 // Profiles
 register_post_type( 'profiles',
	array(
			'labels' => array(
			'name' => ( 'Profiles' ),
			'singular_name' => ( 'Profiles' )
		),
		'public' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'Profiles'),
		'menu_icon' => 'dashicons-admin-users',
		'supports' => array(
		'title', 'editor', 'page-attributes'
			)
		)
);

 // testimonials
  register_post_type( 'service-post',
      array(
          'labels' => array(
              'name' => __( 'Services' ),
              'singular_name' => __( 'Services' )
          ),
          'public' => true,
          'has_archive' => true,
          'hierarchical' => false,
          'rewrite' => array('slug' => 'service-post'),
          'menu_icon' => 'dashicons-lightbulb',
          'supports' => array(
            'title'
          )
      )
  );
   // testimonials
  register_post_type( 'social-media',
      array(
          'labels' => array(
              'name' => __( 'Social Media' ),
              'singular_name' => __( 'Social Media' )
          ),
          'public' => true,
          'has_archive' => true,
          'hierarchical' => false,
          'rewrite' => array('slug' => 'social-media'),
          'menu_icon' => 'dashicons-share',
          'supports' => array(
            'title'
          )
      )
  );
  register_post_type( 'trainings',
      array(
          'labels' => array(
              'name' => __( 'Training' ),
              'singular_name' => __( 'Training' )
          ),
          'public' => true,
          'has_archive' => true,
          'hierarchical' => false,
          'rewrite' => array('slug' => 'trainings'),
          'menu_icon' => 'dashicons-admin-post',
          'supports' => array(
           'title', 'excerpt', 'thumbnail', 'editor'
          )
      )
  );
	}
add_action('init', 'fs_custom_content_types');

function genesis_standard_loop_for_testimonial() {

	// Use old loop hook structure if not supporting HTML5.
	if ( ! genesis_html5() ) {
		genesis_legacy_loop();
		return;
	}

	if ( have_posts() ) :
		echo'<div class="textwidget testimonial">';
		do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();
		if(get_post_meta(get_the_id(),'author_description',true)){
			$author=get_post_meta(get_the_id(),'author_description',true);
		}else{
			$author='';
		}
		$counter++;
			if( $counter % 2 != 0 ) : 
		echo'<div class="one-half first"><div class="quote">'.get_the_content().' </div><div class="quote-arrow"></div><div class="centered">';
			 if( has_post_thumbnail() ) : 
			 the_post_thumbnail();
			  endif;
				echo'<h6>'.get_the_title().'</h6><p class="designation">'.$author.'</p></div></div>';
			else:	
			echo'<div class="one-half"><div class="quote">'.get_the_content().' </div><div class="quote-arrow"></div><div class="centered">';
			 if( has_post_thumbnail() ) : 
			 the_post_thumbnail();
			  endif;
			echo'<h6>'.get_the_title().'</h6><p class="designation">'.$author.'</p></div></div>';
			 endif;
		endwhile; // End of one post.
		echo'</div>';
		do_action( 'genesis_after_endwhile' );

	else : // If no posts exist.
		do_action( 'genesis_loop_else' );
	endif; // End loop.

}

function genesis_standard_loop_for_profile() {
		// Use old loop hook structure if not supporting HTML5.
	if ( ! genesis_html5() ) {
		genesis_legacy_loop();
		return;
	}
	if ( have_posts() ) :
		echo'<div class="textwidget profile">';
		do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();
		$userProfileImage=wp_get_attachment_url(get_post_meta(get_the_id(),'photo',true));
		$counter++;
			if( $counter % 2 != 0 ) : 
					echo'<div class="one-half first"><img src="'.$userProfileImage.'"><h3>';
							the_field('name');
					echo '</h3>';
						the_field('description');
					echo'</div>';
			else:	
					echo'<div class="one-half"><img src="'.$userProfileImage.'"><h3>';
							the_field('name');
					echo '</h3>';
						the_field('description');
					echo'</div>';
			 endif;
		endwhile; // End of one post.
		echo'</div>';
		do_action( 'genesis_after_endwhile' );

	else : // If no posts exist.
		do_action( 'genesis_loop_else' );
	endif; // End loop.
}
function genesis_standard_loop_for_social_media() {
		// Use old loop hook structure if not supporting HTML5.
	if ( ! genesis_html5() ) {
		genesis_legacy_loop();
		return;
	}
	if ( have_posts() ) :
		echo'<div class="textwidget social_media">';
		do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();
		$userProfileImage=wp_get_attachment_url(get_post_meta(get_the_id(),'image',true));
		$counter++;
			if( $counter % 2 != 0 ) : 
					echo'<div class="one-half first"><img src="'.$userProfileImage.'"><h3>';
							the_field('title');
					echo '</h3>';
						the_field('content');
					echo'</div>';
			else:	
					echo'<div class="one-half"><img src="'.$userProfileImage.'"><h3>';
							the_field('title');
					echo '</h3>';
						the_field('content');
					echo'</div>';
			 endif;
		endwhile; // End of one post.
		echo'</div>';
		do_action( 'genesis_after_endwhile' );

	else : // If no posts exist.
		do_action( 'genesis_loop_else' );
	endif; // End loop.
}
function genesis_standard_loop_for_services() {
		// Use old loop hook structure if not supporting HTML5.
	if ( ! genesis_html5() ) {
		genesis_legacy_loop();
		return;
	}
	if ( have_posts() ) :
		echo'<div class="textwidget services">';
		do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();
		$userProfileImage=wp_get_attachment_url(get_post_meta(get_the_id(),'icon',true));
		$counter++;
				if( $counter % 2 != 0 ) : 
					echo'<div class="one-half first"><figure><img src="'.$userProfileImage.'"></figure><h3>';
							the_field('title');
					echo '</h3>';
						the_field('content');
					echo'</div>';
			else:	
					echo'<div class="one-half"><figure><img src="'.$userProfileImage.'"></figure><h3>';
							the_field('title');
					echo '</h3>';
						the_field('content');
					echo'</div>';
			 endif;
					
		endwhile; // End of one post.
		echo'</div>';
		do_action( 'genesis_after_endwhile' );

	else : // If no posts exist.
		do_action( 'genesis_loop_else' );
	endif; // End loop.
}
function genesis_standard_loop_for_training() {
		// Use old loop hook structure if not supporting HTML5.
	if ( ! genesis_html5() ) {
		genesis_legacy_loop();
		return;
	}
	if ( have_posts() ) :
		echo'<div class="textwidget trainig">';
		do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();
		$counter++;
				if( $counter % 2 != 0 ) : ?>
					<div class="one-half first"><figure>
						<?php the_post_thumbnail('full');?>
					</figure>
					<div class="small_content_<?php echo get_the_id()?>" >
											<?php the_excerpt();?>
					<a class="readmore button large" onclick="readmore('.small_content_<?php echo get_the_id()?>','.full_content_<?php  echo get_the_id()?>')">View more</a></div>
					<div class="full_content_<?php echo get_the_id()?>" style="display:none">
											<?php	the_content(); ?>
					<a  class="less_more button large"  onclick="less_more('.small_content_<?php echo get_the_id()?>','.full_content_<?php  echo get_the_id()?>')">Show less</a></div>
					</div>
					<?php else:	?>
					<div class="one-half"><figure>
						<?php the_post_thumbnail('full');?>
					</figure>
					<div class="small_content_<?php echo get_the_id()?>" >
											<?php the_excerpt();?>
					<a  class="readmore button large" onclick="readmore('.small_content_<?php echo get_the_id()?>','.full_content_<?php  echo get_the_id()?>')">View more</a></div>
					<div class="full_content_<?php echo get_the_id()?>" style="display:none">
											<?php	the_content(); ?>
					<a  class="less_more button large" onclick="less_more('.small_content_<?php echo get_the_id()?>','.full_content_<?php  echo get_the_id()?>')">Show less</a></div>
					</div>
			<?php  endif;
					
		endwhile; // End of one post.
		echo'</div>';
		do_action( 'genesis_after_endwhile' );

	else : // If no posts exist.
		do_action( 'genesis_loop_else' );
	endif; // End loop.
}
function genesis_custom_loop_for_testimonial( $args = array() ) {

	global $wp_query, $more;

	$defaults = array(); // For forward compatibility.
	$args     = apply_filters( 'genesis_custom_loop_args', wp_parse_args( $args, $defaults ), $args, $defaults );

	$wp_query = new WP_Query( $args );
	
	// Only set $more to 0 if we're on an archive.
	$more = is_singular() ? $more : 0;
		if($args['post_type']=='profiles'){
			genesis_standard_loop_for_profile();
			
		}elseif($args['post_type']=='testimonials'){
			genesis_standard_loop_for_testimonial();
		}elseif($args['post_type']=='social-media'){
			genesis_standard_loop_for_social_media();
		}elseif($args['post_type']=='service-post'){
			genesis_standard_loop_for_services();
		}elseif($args['post_type']=='trainings'){
			genesis_standard_loop_for_training();
		}
		else{
			
		}
	

	// Restore original query.
	wp_reset_query();

}
 // testimonials



if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-options',
		
	));
	
}
remove_action('genesis_footer', 'genesis_do_footer');
add_action('genesis_footer', 'genesis_do_footers');

function genesis_do_footers() {

	// Build the text strings. Includes shortcodes.
	if(get_option(options_footer_logo)){
		$image_attributes = wp_get_attachment_image_src(get_option(options_footer_logo),'full',true);
		$footer_text = '<div class="gototop footer-section-logo"><img src="'.$image_attributes[0].'"/></div>';
	}else{
		$footer_text = '';
	}
	if(get_option(options_australian_business_number_abn)){
		$abn_number='| '.get_option('options_australian_business_number_abn');
	}else{
		$abn_number='';
	}
	if(get_option(options_copyright_configurable_name)){
		$copyright_name=get_option('options_copyright_configurable_name');
	}else{
		$copyright_name='';
	}
	if(get_option(options_address)){
		$address='<p class="footer_address"> Address : '.get_option('options_address').'</p>';
	}else{
		$address='';
	}
	$backtotop_text = '[footer_backtotop]';
	$creds_text     = $footer_text.$address.'Copyright'.do_shortcode('[footer_copyright]').'&nbsp;'.$copyright_name.' All Rights Reserved'.$abn_number;

	// Filter the text strings.
	$backtotop_text = apply_filters( 'genesis_footer_backtotop_text', $backtotop_text );
	$creds_text     = apply_filters( 'genesis_footer_creds_text', $creds_text );

	$backtotop = $backtotop_text ? sprintf( '<div class="gototop"><p>%s</p></div>', $backtotop_text ) : '';
	
	$creds     = $creds_text ? sprintf( '<div class="creds"><p>%s</p></div>', $creds_text ) : '';

	$output = $backtotop . $creds;

	// Only use credits if HTML5.
	/*if ( genesis_html5() ) {
		$output = '<p>' . genesis_strip_p_tags( $creds_text ) . '</p>';
	}*/

	echo apply_filters( 'genesis_footer_output', $output, $backtotop_text, $creds_text );

}
function case_study(){
	echo'<div class="entry-content entry-portfolio" itemprop="text"><h3 class="casestudy_title">'.get_field('case_study_title','option').'</h3>
</div><div class="textwidget">';
	if( have_rows('case_studies_item','option') ):
	$j=1;
		  while ( have_rows('case_studies_item','option')) : the_row();
		  $image=get_sub_field('case_study_image');
		  if($j==1){
			  echo'<div class="one-third first BlogList">
				<a href="'.get_sub_field('case_study_url').'" target="_blank">
					<img src="'.$image["url"].'">
				</a>
			</div>';
		  }else{
			 echo'<div class="one-third BlogList">
				<a href="'.get_sub_field('case_study_url').'" target="_blank">
					<img src="'.$image["url"].'">
				</a>
			</div>'; 
		  }
			
		$j++;
		endwhile;
	endif;
echo'</div>';
}
function genesis_custom_loop_for_profile( $args = array() ) {

	global $wp_query, $more;

	$defaults = array(); // For forward compatibility.
	$args     = apply_filters( 'genesis_custom_loop_args', wp_parse_args( $args, $defaults ), $args, $defaults );

	$wp_query = new WP_Query( $args );
	
	// Only set $more to 0 if we're on an archive.
	$more = is_singular() ? $more : 0;
		if ( ! genesis_html5() ) {
		genesis_legacy_loop();
		return;
	}
	if ( have_posts() ) :
		echo'<div class="textwidget profile"><div class="wrap">';
		do_action( 'genesis_before_while' );
		$i=0;
		while ( have_posts() ) : the_post();
		$userProfileImage=wp_get_attachment_url(get_post_meta(get_the_id(),'photo',true));
		$counter++;
		if($i!=0 && $i%3==0):
		echo'</div><div class="wrap">';
		endif;
		?>
					
					<div class="one-third custom_profile">
							<img src="<?= $userProfileImage;?>">
							<h3>
								<?= the_field('name');?>
							</h3>
							<div class="small_content_<?php echo get_the_id()?>" >
								<?php	the_field('excerpt'); ?>
							<a class="readmore button large" onclick="readmore('.small_content_<?php echo get_the_id()?>','.full_content_<?php  echo get_the_id()?>')">
								 View more <i class="fa fa-angle-down" aria-hidden="true"></i>
								</a>		
								
							</div>
							<div class="full_content_<?php echo get_the_id()?>" style="display:none">
								<?php	the_field('description'); ?>
						<a  class="less_more button large"  onclick="less_more('.small_content_<?php echo get_the_id()?>','.full_content_<?php  echo get_the_id()?>')">
								Show less <i class="fa fa-angle-up" aria-hidden="true"></i>
								</a>		
								
							</div>
					</div>
		
			
			 <?php
			 $i++;
		endwhile; // End of one post.
		echo'</div></div>';
		do_action( 'genesis_after_endwhile' );

	else : // If no posts exist.
		do_action( 'genesis_loop_else' );
	endif; // End loop.
	// Restore original query.
	wp_reset_query();

}