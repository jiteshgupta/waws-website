<?php

// Template Name: Profile

// Adds Page Title
//add_action( 'genesis_before_content', 'genesis_do_post_title' );

//Adds Page Content
add_action( 'genesis_before_loop', 'aspire_do_portfolio_content' );
function aspire_do_portfolio_content() {
      echo '<div class="entry-content entry-portfolio" itemprop="text">';
		while ( have_posts() ) : the_post();
			the_content();
		endwhile;
	  echo '</div>';
}
		
// Force layout to full-width-content
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );


// Remove standard loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

// Add custom loop
add_action( 'genesis_loop', 'profile_loop' );
function profile_loop() {
    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$cf = genesis_get_custom_field( 'query_args' ); // Easter Egg
   $args = array('showposts' => genesis_get_option( 'aspire_portfolio_cat_num' ),
	'paged' => $paged,
	'post_type'=>'profiles',
	'orderby' => 'menu_order', 
    'order' => 'ASC', 
	'meta_query' => array(
							array(
							 'key' => 'top_profile',
							 'value' => 1
							),
					)
	);
    $query_args = wp_parse_args($cf, $args);
    genesis_custom_loop_for_testimonial( $query_args );
    echo'<div class="clear-both"></div><div class="bottom-content">
			<h3><strong>MEET THE TEAM</strong></h3>
	</div>';
	 $args_custom = array('showposts' => genesis_get_option( 'aspire_portfolio_cat_num' ),
	'paged' => $paged,
	'post_type'=>'profiles',
	'orderby' => 'menu_order', 
    'order' => 'ASC', 
	'meta_query' => array(
							array(
							 'key' => 'top_profile',
							 'value' => 0
							),
					)
	);
    $query_args_custom = wp_parse_args($cf, $args_custom);
    genesis_custom_loop_for_profile( $query_args_custom );
	echo'<div class="clear-both"></div><div class="bottom-content">';
			the_field('bottom_content');
	echo'</div>';

}
	
?> <body <?php body_class('portfolio'); ?>> 

<?php
	
genesis();		
