<?php

// Template Name: Blog

// Adds Page Title
//add_action( 'genesis_before_content', 'genesis_do_post_title' );

//Adds Page Content
add_action( 'genesis_before_loop', 'aspire_do_portfolio_content' );
function aspire_do_portfolio_content() {
   echo '<div class="entry-content entry-portfolio" itemprop="text">' . get_post()->post_content . '</div>';
}
		
// Force layout to full-width-content
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );


// Remove standard loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

// Add custom loop
add_action( 'genesis_loop', 'blog_loop' );
function blog_loop() {
   genesis_do_loop();
   echo'<div class="clear-both"></div><div class="bottom-content">';
			the_field('bottom_content');
	echo'</div>';
}
	
?> <body <?php body_class('Blog'); ?>> 

<?php
	
genesis();		